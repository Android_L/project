package info.elearning.toefl.app;

public class AppConfig {
	// Server user login url
	public static String URL_LOGIN = "http://mtoefl-api.hol.es/login.php";

	// Server user register url
	public static String URL_REGISTER = "http://mtoefl-api.hol.es/register.php";

	//server writting

	public static String URL_WRITTING_SELECTED = "http://mtoefl-api.hol.es/detail_materi_writting.php";
	public static String URL_QUIZ = "http://mtoefl-api.hol.es/quiz/index.php";
}
