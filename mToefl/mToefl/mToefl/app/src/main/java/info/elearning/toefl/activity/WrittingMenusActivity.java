package info.elearning.toefl.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.ByteArrayOutputStream;

import info.elearning.toefl.R;

public class WrittingMenusActivity extends AppCompatActivity {
    Button aux;
    Button comp;
    Button pre;
    Button cc;
    Button ne;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.writting_menus);


        //declare variable
        aux = (Button) findViewById(R.id.aux);
        comp = (Button) findViewById(R.id.comp);
        pre = (Button) findViewById(R.id.pre);
        cc = (Button) findViewById(R.id.cc);
        ne = (Button) findViewById(R.id.ne);

        //action button

        aux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.materi_aux);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG,Bitmap.DENSITY_NONE,byteArrayOutputStream);
                byte[] b = byteArrayOutputStream.toByteArray();


                Intent in = new Intent(getApplicationContext(),Writting_activity.class);
                in.putExtra("title","Auxiliary Verb");
                in.putExtra("keterangan",getResources().getString(R.string.keterangan_aux));
                in.putExtra("gambar",b);
                startActivity(in);

            }
        });

        comp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.comp);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG,Bitmap.DENSITY_NONE,byteArrayOutputStream);
                byte[] b = byteArrayOutputStream.toByteArray();

                Intent in = new Intent(getApplicationContext(),Writting_activity.class);
                in.putExtra("title","Comparation");
                in.putExtra("keterangan", getResources().getString(R.string.keterangan_comp));
                in.putExtra("gambar",b);
                startActivity(in);
            }
        });

        pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.white);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG,Bitmap.DENSITY_NONE,byteArrayOutputStream);
                byte[] b = byteArrayOutputStream.toByteArray();

                Intent in = new Intent(getApplicationContext(),Writting_activity.class);
                in.putExtra("title","Preposition");
                in.putExtra("keterangan",getResources().getString(R.string.keterangan_pre));
                in.putExtra("gambar",b);

                startActivity(in);
            }
        });

        cc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.cc);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG,Bitmap.DENSITY_NONE,byteArrayOutputStream);
                byte[] b = byteArrayOutputStream.toByteArray();

                Intent in = new Intent(getApplicationContext(),Writting_activity.class);
                in.putExtra("title","Correlative Conjunction");
                in.putExtra("keterangan",getResources().getString(R.string.keterangan_cc));
                in.putExtra("gambar",b);
                startActivity(in);
            }
        });

        ne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.white);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG,Bitmap.DENSITY_NONE,byteArrayOutputStream);
                byte[] b = byteArrayOutputStream.toByteArray();

                Intent in = new Intent(getApplicationContext(),Writting_activity.class);
                in.putExtra("title","Negative Emphasis");
                in.putExtra("keterangan",getResources().getString(R.string.keterangan_ne));
                in.putExtra("gambar",b);

                startActivity(in);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent in = getIntent();
        in.removeExtra("gambar");
        in.removeExtra("title");
        in.removeExtra("keterangan");

        super.onBackPressed();
    }
}
