package info.elearning.toefl.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import info.elearning.toefl.R;
import info.elearning.toefl.app.AppConfig;
import info.elearning.toefl.app.QuizWrapper;

public class Test_activity extends AppCompatActivity {
    TextView level;
    TextView quizQuestion;
    RadioGroup radioGroup;
    RadioButton optionOne;
    RadioButton optionTwo;
    RadioButton optionThree;
    RadioButton optionFour;
    RadioButton optionFive;
    int currentQuizQuestion;
    int quizCount;
    QuizWrapper firstQuestion;
    List<QuizWrapper> parseObject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        level = (TextView) findViewById(R.id.level);
        quizQuestion = (TextView) findViewById(R.id.soal);
        radioGroup = (RadioGroup) findViewById(R.id.jawaban);
        optionOne = (RadioButton) findViewById(R.id.a);
        optionTwo = (RadioButton) findViewById(R.id.b);
        optionThree = (RadioButton) findViewById(R.id.c);
        optionFour = (RadioButton) findViewById(R.id.d);
        optionFive = (RadioButton) findViewById(R.id.e);

        Button prev = (Button) findViewById(R.id.prev_soal);
        Button confirm = (Button) findViewById(R.id.confirm);
        final Button next = (Button) findViewById(R.id.next_soal);


        AsyncJsonObject asyncJsonObject = new AsyncJsonObject();
        asyncJsonObject.execute("");

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                next.setVisibility(View.VISIBLE);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int radioSelected = radioGroup.getCheckedRadioButtonId();
                int userSelection = getSelectedAnswer(radioSelected);
                int correctAnswerForQuestion = firstQuestion.getCorrectAnswer();
                if (userSelection == correctAnswerForQuestion){
                    Toast.makeText(Test_activity.this, "Correct", Toast.LENGTH_SHORT).show();
                    currentQuizQuestion++;
                    if (currentQuizQuestion >= quizCount){
                        Toast.makeText(Test_activity.this, "End", Toast.LENGTH_SHORT).show();
                        return;
                    } else{
                        firstQuestion = parseObject.get(currentQuizQuestion);

                        quizQuestion.setText(firstQuestion.getQuestion());
                        String[] possibleAnswers = firstQuestion.getAnswers().split(",");

                        optionOne.setText(possibleAnswers[0]);
                        optionTwo.setText(possibleAnswers[1]);
                        optionThree.setText(possibleAnswers[2]);
                        optionFour.setText(possibleAnswers[3]);
                        optionFive.setText(possibleAnswers[4]);

                    }
                }else{
                    //failed
                    Toast.makeText(Test_activity.this, "Wrong answer", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });



    }

    private class AsyncJsonObject extends AsyncTask<String,Void,String>{

        private ProgressDialog progressDialog;
        int qid = 1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(Test_activity.this,"Downloading Quiz","wait...",true);
        }

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpPost httpPost = new HttpPost(AppConfig.URL_QUIZ);
            String jsonResult ="";
            try{
                HttpResponse response = httpClient.execute(httpPost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                System.out.println("Returned Json Object "+jsonResult.toString());

            }catch (ClientProtocolException e){
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return jsonResult;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            System.out.println("Result Value: "+s);
            parseObject = returnParsedJsonObject(s);

            if (parseObject == null){
                return;
            }

            quizCount = parseObject.size();
            firstQuestion = parseObject.get(0);
            quizQuestion.setText(firstQuestion.getQuestion());
            String[] possibleAnswers = firstQuestion.getAnswers().split(",");
            optionOne.setText(possibleAnswers[0]);
            optionTwo.setText(possibleAnswers[1]);
            optionThree.setText(possibleAnswers[2]);
            optionFour.setText(possibleAnswers[3]);
            optionFive.setText(possibleAnswers[4]);
        }

        private StringBuilder inputStreamToString(InputStream is){
            String rLine = "";
            StringBuilder answer = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            try{
                while((rLine = br.readLine()) != null){
                    answer.append(rLine);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return answer;
        }
    }

    private List<QuizWrapper> returnParsedJsonObject(String result){
        List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();
        JSONObject resultObject = null;
        JSONArray jsonArray = null;
        QuizWrapper newItemObject = null;

        try{
            resultObject = new JSONObject(result);
            System.out.println("Testing the water "+resultObject.toString());
            jsonArray = resultObject.optJSONArray("quiz");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0;i < jsonArray.length();i++){
            JSONObject jsonChildNode = null;
            try{
                jsonChildNode = jsonArray.getJSONObject(i);
                int id = jsonChildNode.getInt("id");
                String question = jsonChildNode.getString("question");
                String answerOptions = jsonChildNode.getString("possible_answers");
                int correctAnswer = jsonChildNode.getInt("correct_answer");
                newItemObject = new QuizWrapper(id,question,answerOptions,correctAnswer);
                jsonObject.add(newItemObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }

    private int getSelectedAnswer(int radioSelected){
        int answerSelected = 0;
        if (radioSelected == R.id.a){
            answerSelected = 1;
        }
        if (radioSelected == R.id.b){
            answerSelected = 2;
        }
        if (radioSelected == R.id.c){
            answerSelected = 3;
        }
        if (radioSelected == R.id.d){
            answerSelected = 4;
        }
        if (radioSelected == R.id.e){
            answerSelected = 5;
        }
        return answerSelected;
    }


}
