package info.elearning.toefl.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import info.elearning.toefl.R;
import layout.FCTFragment;
import layout.FPFCTFragment;
import layout.FPFTFragment;
import layout.FPSCTFragment;
import layout.FPSPFCTFragment;
import layout.FPSPFTFragment;
import layout.FPSTFragment;
import layout.FutureFragment;
import layout.PCTFragment;
import layout.PPCTFragment;
import layout.PPTFragment;
import layout.PSCTFragment;
import layout.PSPFCTFragment;
import layout.PSPFTFragment;
import layout.PastFragment;
import layout.PresentFragment;

public class Grammar_activity extends AppCompatActivity {
    Button past;Button future;Button present;Button pc;Button pp;Button ppc;Button psc;
    Button pspf;Button pspfc;Button fc;Button fps;Button fpf;Button fpsc;Button fpfc;
    Button fpspf;Button fpspfc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grammar_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        past = (Button) findViewById(R.id.past_btn);
        future = (Button) findViewById(R.id.future_btn);
        present = (Button) findViewById(R.id.present_btn);
        pc = (Button) findViewById(R.id.pc_btn);
        ppc = (Button) findViewById(R.id.ppc_btn);
        psc = (Button) findViewById(R.id.psc_btn);
        pspf = (Button) findViewById(R.id.pspf_btn);
        pspfc = (Button) findViewById(R.id.pspfc_btn);
        fc = (Button) findViewById(R.id.fc_btn);
        fps = (Button) findViewById(R.id.fps_btn);
        fpf = (Button) findViewById(R.id.fpf_btn);
        fpsc = (Button) findViewById(R.id.fpsc_btn);
        fpfc = (Button) findViewById(R.id.fpfc_btn);
        fpspf = (Button) findViewById(R.id.fpspf_btn);
        fpspfc = (Button) findViewById(R.id.fpspfc_btn);
        pp = (Button) findViewById(R.id.ppc_btn);

        final RelativeLayout r1 = (RelativeLayout) findViewById(R.id.r1);

        //action past button
        past.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PastFragment fragment = new PastFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action past perfect button
        pspf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PSPFTFragment fragment = new PSPFTFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action future button
        future.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FutureFragment fragment = new FutureFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action present button
        present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PresentFragment fragment = new PresentFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action Future Continous button
        fc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FCTFragment fragment = new FCTFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action future perfect continous button
        fpfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FPFCTFragment fragment = new FPFCTFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action future perfect button
        fpf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FPFTFragment fragment = new FPFTFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action future past continous button
        fpsc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FPSCTFragment fragment = new FPSCTFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action future past perfect continous button
        past.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FPSPFCTFragment fragment = new FPSPFCTFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action future past perfect button
        fpspf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FPSPFTFragment fragment = new FPSPFTFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action future past button
        fps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FPSTFragment fragment = new FPSTFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action present continous button
        pc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PCTFragment fragment = new PCTFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action present perfect continous button
        ppc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PPCTFragment fragment = new PPCTFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action present perfect button
        pp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PPTFragment fragment = new PPTFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action past continous button
        psc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PSCTFragment fragment = new PSCTFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });

        //action past perfect continous button
        pspfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PSPFCTFragment fragment = new PSPFCTFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                r1.setVisibility(View.INVISIBLE);
                ft.commit();
            }
        });
    }

    @Override
    public void onBackPressed() {

        RelativeLayout r1 = (RelativeLayout) findViewById(R.id.r1);
        if (r1.isShown()){
            super.onBackPressed();
        } else{
            r1.setVisibility(View.VISIBLE);
        }

    }
}
