package info.elearning.toefl.activity;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import info.elearning.toefl.R;
import layout.FCTFragment;
import layout.FPFCTFragment;
import layout.FPFTFragment;
import layout.FPSCTFragment;
import layout.FPSPFCTFragment;
import layout.FPSPFTFragment;
import layout.FPSTFragment;
import layout.FutureFragment;
import layout.PCTFragment;
import layout.PPCTFragment;
import layout.PPTFragment;
import layout.PSCTFragment;
import layout.PSPFCTFragment;
import layout.PSPFTFragment;
import layout.PastFragment;
import layout.PresentFragment;

/**
 * A placeholder fragment containing a simple view.
 */
public class Grammar_activityFragment extends Fragment {

    Button past;Button future;Button present;Button pc;Button pp;Button ppc;Button psc;
    Button pspf;Button pspfc;Button fc;Button fps;Button fpf;Button fpsc;Button fpfc;
    Button fpspf;Button fpspfc;

    public Grammar_activityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = View.inflate(getContext(),R.layout.fragment_grammar_activity,null);
        //declare variable
        past = (Button) v.findViewById(R.id.past_btn);
        future = (Button) v.findViewById(R.id.future_btn);
        present = (Button) v.findViewById(R.id.present_btn);
        pc = (Button) v.findViewById(R.id.pc_btn);
        ppc = (Button) v.findViewById(R.id.ppc_btn);
        psc = (Button) v.findViewById(R.id.psc_btn);
        pspf = (Button) v.findViewById(R.id.pspf_btn);
        pspfc = (Button) v.findViewById(R.id.pspfc_btn);
        fc = (Button) v.findViewById(R.id.fc_btn);
        fps = (Button) v.findViewById(R.id.fps_btn);
        fpf = (Button) v.findViewById(R.id.fpf_btn);
        fpsc = (Button) v.findViewById(R.id.fpsc_btn);
        fpfc = (Button) v.findViewById(R.id.fpfc_btn);
        fpspf = (Button) v.findViewById(R.id.fpspf_btn);
        fpspfc = (Button) v.findViewById(R.id.fpspfc_btn);
        pp = (Button) v.findViewById(R.id.ppc_btn);

        //action past button
        past.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PastFragment fragment = new PastFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                ft.commit();
            }
        });

        //action past perfect button
        pspf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PSPFTFragment fragment = new PSPFTFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                ft.commit();
            }
        });

        //action future button
        future.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FutureFragment fragment = new FutureFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                ft.commit();
            }
        });

        //action present button
        present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PresentFragment fragment = new PresentFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                ft.commit();
            }
        });

        //action Future Continous button
        fc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FCTFragment fragment = new FCTFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                ft.commit();
            }
        });

        //action future perfect continous button
        fpfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FPFCTFragment fragment = new FPFCTFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                ft.commit();
            }
        });

        //action future perfect button
        fpf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FPFTFragment fragment = new FPFTFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                ft.commit();
            }
        });

        //action future past continous button
        fpsc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FPSCTFragment fragment = new FPSCTFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                ft.commit();
            }
        });

        //action future past perfect continous button
        past.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FPSPFCTFragment fragment = new FPSPFCTFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                ft.commit();
            }
        });

        //action future past perfect button
        fpspf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FPSPFTFragment fragment = new FPSPFTFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                ft.commit();
            }
        });

        //action future past button
        fps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FPSTFragment fragment = new FPSTFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                ft.commit();
            }
        });

        //action present continous button
        pc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PCTFragment fragment = new PCTFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                ft.commit();
            }
        });

        //action present perfect continous button
        ppc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PPCTFragment fragment = new PPCTFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                ft.commit();
            }
        });

        //action present perfect button
        pp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PPTFragment fragment = new PPTFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                ft.commit();
            }
        });

        //action past continous button
        psc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PSCTFragment fragment = new PSCTFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                ft.commit();
            }
        });

        //action past perfect continous button
        pspfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PSPFCTFragment fragment = new PSPFCTFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container,fragment);
                ft.commit();
            }
        });

        return inflater.inflate(R.layout.fragment_grammar_activity, container, false);
    }
}
