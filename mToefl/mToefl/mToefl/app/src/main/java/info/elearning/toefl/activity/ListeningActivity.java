package info.elearning.toefl.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

import info.elearning.toefl.R;

public class ListeningActivity extends AppCompatActivity {

    private MediaPlayer mp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listening);

        //declarated button
        Button lesson1 = (Button) findViewById(R.id.lesson1);
        Button lesson2 = (Button) findViewById(R.id.lesson2);
        Button lesson3 = (Button) findViewById(R.id.lesson3);
        Button lesson4 = (Button) findViewById(R.id.lesson4);
        Button lesson5 = (Button) findViewById(R.id.lesson5);
        Button lesson6 = (Button) findViewById(R.id.lesson6);
        Button lesson7 = (Button) findViewById(R.id.lesson7);
        Button lesson8 = (Button) findViewById(R.id.lesson8);
        Button lesson9 = (Button) findViewById(R.id.lesson9);
        Button lesson10 = (Button)findViewById(R.id.lesson10);

        //function button
        //Button 1
        lesson1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                mp = MediaPlayer.create(ListeningActivity.this,R.raw.a_fun_day);
                mp.start();


            }
        });

        //Button 2
        lesson2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                mp = MediaPlayer.create(ListeningActivity.this,R.raw.a_story_to_remember);
                mp.start();

            }
        });

        //Button 3
        lesson3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                mp = MediaPlayer.create(ListeningActivity.this,R.raw.baking_cookies);
                mp.start();
            }
        });

        //Button 4
        lesson4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                mp = MediaPlayer.create(ListeningActivity.this,R.raw.meeting_singles);
                mp.start();
            }
        });

        //Button 5
        lesson5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                mp = MediaPlayer.create(ListeningActivity.this,R.raw.dating_woes);
                mp.start();

            }
        });

        //Button 6
        lesson6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                mp = MediaPlayer.create(ListeningActivity.this,R.raw.restaurants);
                mp.start();

            }
        });

        //Button 7
        lesson7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                mp = MediaPlayer.create(ListeningActivity.this,R.raw.joes_hamburger_restaurant);
            }
        });

        //Button 8
        lesson8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                mp = MediaPlayer.create(ListeningActivity.this,R.raw.saturdays_chores);
                mp.start();
            }
        });

        //Button 9
        lesson9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                mp = MediaPlayer.create(ListeningActivity.this,R.raw.friday_night_mishap);
                mp.start();
            }
        });

        //Button 10
        lesson10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                mp = MediaPlayer.create(ListeningActivity.this,R.raw.movies);
                mp.start();
            }
        });
    }

    private void stopPlaying() {
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopPlaying();
    }
}
