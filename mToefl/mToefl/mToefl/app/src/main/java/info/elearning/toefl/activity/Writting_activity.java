package info.elearning.toefl.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import info.elearning.toefl.R;

public class Writting_activity extends AppCompatActivity {
    public TextView title;
    public TextView use;
    private ImageView gambar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.writting_activity);

        //declare variable
        title = (TextView) findViewById(R.id.judul);
        use = (TextView) findViewById(R.id.keterangan);
        gambar = (ImageView) findViewById(R.id.gambar);


        //action
        Intent in = getIntent();
        Bundle b = in.getExtras();

        byte[] pic = b.getByteArray("gambar");
        Bitmap bmp = BitmapFactory.decodeByteArray(pic,0,pic.length);

        String t = (String) b.get("title");
        String u = (String) b.get("keterangan");
        gambar.setImageBitmap(bmp);
        title.setText(t);
        use.setText(u);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public void reset_gambar(TextView a, TextView b, ImageView c){
        gambar = c;
        title = a;
        use = b;
        a.setText("");
        b.setText("");
        c.setImageBitmap(null);
    }


}
