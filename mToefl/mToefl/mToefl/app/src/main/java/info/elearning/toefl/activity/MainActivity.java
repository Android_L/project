package info.elearning.toefl.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.transition.Slide;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;


import java.util.HashMap;

import info.elearning.toefl.R;
import info.elearning.toefl.helper.SQLiteHandler;
import info.elearning.toefl.helper.SessionManager;

public class MainActivity extends Activity {

	private TextView txtName;
	private TextView txtEmail;
	private Button btnLogout;
	private Button btnWriting;
	private Button btnReading;
	private Button btnListening;
	private Button btnGrammar;
	private Button btnKamus;
	private Button btnTest;

	private SQLiteHandler db;
	private SessionManager session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setupWindowAnimations();

		//declaration Object
		txtName = (TextView) findViewById(R.id.name);
		txtEmail = (TextView) findViewById(R.id.email);
		//btnLogout = (Button) findViewById(R.id.btnLogout);

        Button_active();

		// SqLite database handler
		db = new SQLiteHandler(getApplicationContext());

		// session manager
		session = new SessionManager(getApplicationContext());

		if (!session.isLoggedIn()) {
			//logoutUser();
		}

		// Fetching user details from SQLite
		HashMap<String, String> user = db.getUserDetails();

		String name = user.get("name");
		String email = user.get("email");

		// Displaying the user details on the screen
		txtName.setText(name);
		txtEmail.setText(email);

		// Logout button click event

		//btnLogout.setOnClickListener(new View.OnClickListener() {

//			@Override
//			public void onClick(View v) {
//						logoutUser();
//			}
//		});
	}

	/**
	 * Logging out the user. Will set isLoggedIn flag to false in shared
	 * preferences Clears the user data from sqlite users table
	 * */

	private void logoutUser() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Log Out");
		builder.setMessage("Are You Sure?");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				session.setLogin(false);

				db.deleteUsers();

				// Launching the login activity
				Intent intent = new Intent(MainActivity.this, LoginActivity.class);
				overridePendingTransition(R.anim.fadein, R.anim.fadeout);
				startActivity(intent);

				finish();

				dialog.dismiss();
			}
		});

		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();


	}


	/**
	 * Procedure Exit Application
	 */
	@Override
	public void onBackPressed() {
		ExitApp();
	}

	private void ExitApp(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Exit Application");
		builder.setMessage("Are You Sure?");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				System.exit(0);
				dialog.dismiss();
			}
		});

		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

    private void Button_active(){
        btnReading = (Button) findViewById(R.id.reading);
        btnWriting = (Button) findViewById(R.id.writing);
        btnKamus = (Button) findViewById(R.id.kamus);
        btnGrammar = (Button) findViewById(R.id.grammar);
        btnListening = (Button) findViewById(R.id.listening);
        btnTest = (Button) findViewById(R.id.test);

        //button reading clicked
        btnReading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),reading_list.class);
				overridePendingTransition(R.anim.push_down_in,R.anim.push_down_out);
                startActivity(i);
            }
        });

        //button writting clicked
        btnWriting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),WrittingMenusActivity.class);
				overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
                startActivity(i);
            }
        });

        //button kamus clicked
        btnKamus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),Kamus_activity.class);
                startActivity(i);
            }
        });

        //button grammar clicked
        btnGrammar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),Grammar_activity.class);
				//getWindow().getAttributes().windowAnimations = R.style.Fade;
                startActivity(i);
            }
        });

        //button listening clicked
        btnListening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),ListeningActivity.class);
                startActivity(i);
            }
        });

        //button test clicked
        btnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),Test_activity.class);
                startActivity(i);

            }
        });

    }

	private void setupWindowAnimations() {
		Slide slide = (Slide) TransitionInflater.from(this).inflateTransition(R.transition.slide);
		getWindow().setExitTransition(slide);
	}

}
