package info.elearning.toefl.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import info.elearning.toefl.R;

/**
 * Created by elham on 16/05/2016.
 */
public class reading_list extends AppCompatActivity {

    Button nara;
    Button des;
    Button pro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reading_list);

        //declare variable
        nara = (Button) findViewById(R.id.narative);
        des = (Button) findViewById(R.id.deskripsi);
        pro = (Button) findViewById(R.id.procedure);


        //button action
        nara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(),Reading_activity.class);
                in.putExtra("isi",getResources().getString(R.string.narative));
                startActivity(in);

            }
        });

        des.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(),Reading_activity.class);
                in.putExtra("isi",getResources().getString(R.string.deskriptif));
                startActivity(in);
            }
        });

        pro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(),Reading_activity.class);
                in.putExtra("isi",getResources().getString(R.string.prosedur));
                startActivity(in);
            }
        });



    }
}
