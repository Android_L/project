package info.elearning.toefl.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wordsapi.www.client.*;
import com.wordsapi.www.wordsapi.api.*;
import com.wordsapi.www.wordsapi.model.*;


import info.elearning.toefl.R;

public class Kamus_activity extends AppCompatActivity {

    EditText cari;
    Button search;
    TextView hasil;
    String wordToken="UfOMByLztTPMxWuEYv9kZnxFa0";
    String detail = "definitions";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kamus_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        cari = (EditText) findViewById(R.id.kamus_txt);
        search = (Button) findViewById(R.id.search_btn);
        hasil = (TextView) findViewById(R.id.hasil_kamus);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    WordsApi wordsApi = new WordsApi();
                    DetailsResponse response = wordsApi.details(wordToken, cari.getText().toString(),detail);
                    hasil.setText(String.valueOf(response));
                }catch (ApiException e){
                    Toast.makeText(Kamus_activity.this, "Error "+e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
