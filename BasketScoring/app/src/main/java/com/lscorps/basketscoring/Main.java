package com.lscorps.basketscoring;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class Main extends AppCompatActivity {

    Button triA;
    Button triB;
    Button twoA;
    Button twoB;
    Button oneA;
    Button oneB;
    Button reset;
    TextView scoreA;
    TextView scoreB;
    int ScoreA = 0;
    int ScoreB = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Button Event
        ButtonFunction();

    }

    private void ButtonFunction(){
        triA = (Button) findViewById(R.id.triA);
        triB = (Button) findViewById(R.id.triB);
        twoA = (Button) findViewById(R.id.duaA);
        twoB = (Button) findViewById(R.id.duaB);
        oneA = (Button) findViewById(R.id.oneA);
        oneB = (Button) findViewById(R.id.oneB);
        reset = (Button) findViewById(R.id.reset);

        /**
         * Score Team A
         */
        triA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScoreA += 3;
                displayScoreTeamA(ScoreA);
            }
        });

        twoA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScoreA +=2;
                displayScoreTeamA(ScoreA);
            }
        });

        oneA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScoreA +=1;
                displayScoreTeamA(ScoreA);
            }
        });

        /**
         * Score team B
         */
        triB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScoreB +=3;
                displayScoreTeamB(ScoreB);
            }
        });

        twoB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScoreB +=2;
                displayScoreTeamB(ScoreB);
            }
        });

        oneB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScoreB +=1;
                displayScoreTeamB(ScoreB);
            }
        });

        /**
         * Reset button event
         */
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScoreA = 0;
                ScoreB = 0;
                displayScoreTeamA(ScoreA);
                displayScoreTeamB(ScoreB);
            }
        });
    }

    private void displayScoreTeamA(int number){
        scoreA = (TextView) findViewById(R.id.scoreTeamA);
        scoreA.setText(""+number);
    }

    private void displayScoreTeamB(int number){
        scoreB = (TextView) findViewById(R.id.scoreTeamB);
        scoreB.setText(""+number);
    }


}
