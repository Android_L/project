package com.lscorps.justjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class Main extends AppCompatActivity {

    int numberOfCoffees = 0;
    TextView price;
    CheckBox gula;
    CheckBox susu;
    CheckBox jahe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    //mengurangi jumlah pesanan
    public void kurang(View view){
        numberOfCoffees -=1;
        if (numberOfCoffees <= 0) {
            Toast.makeText(this, "Kamu belum pesan kopi", Toast.LENGTH_LONG).show();
            numberOfCoffees = 0;
            display(numberOfCoffees);
        }
        display(numberOfCoffees);
    }

    //menambah jumlah pesanan
    public void tambah(View view){
        numberOfCoffees +=1;
        display(numberOfCoffees);
    }

    //menampilkan jumlah pesanan
    public void display(int number){
        TextView quantity = (TextView) findViewById(R.id.numberOfCoffees);
        quantity.setText("" + number);
    }

    //menghitung jumlah pesanan
    public void order(View view){
        price = (TextView) findViewById(R.id.price);
        price.setText("kamu memesan\n" + numberOfCoffees + " kopi" + "\ndengan harga\n" + "$" + numberOfCoffees * 5);

    }

    //mereset jumlah pesanan
    public void reset(View view){
        numberOfCoffees = 0;
        price = (TextView) findViewById(R.id.price);
        display(numberOfCoffees);
        price.setText("");
    }

}
